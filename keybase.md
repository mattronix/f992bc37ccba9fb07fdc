### Keybase proof

I hereby claim:

  * I am mattronix on github.
  * I am mattronix (https://keybase.io/mattronix) on keybase.
  * I have a public key whose fingerprint is 4B40 7AE9 F989 204F 4F96  92C4 3C82 E5A8 D2E8 DE01

To claim this, I am signing this object:

```json
{
    "body": {
        "client": {
            "name": "keybase.io node.js client",
            "version": "0.7.7"
        },
        "key": {
            "fingerprint": "4b407ae9f989204f4f9692c43c82e5a8d2e8de01",
            "host": "keybase.io",
            "key_id": "3C82E5A8D2E8DE01",
            "uid": "dc51eda9132dd566f4c8ea2180016a00",
            "username": "mattronix"
        },
        "merkle_root": {
            "ctime": 1424800260,
            "hash": "94eb41243bfd13481ebe3cbc11a5a3ba488b934e4d2b249e0a1f2340bff9524ad1d2d9ec2e51593c23ee690a967dfcc7668dcbb8e8c8e07d91cf9ed902f4d71a",
            "seqno": 160733
        },
        "service": {
            "name": "github",
            "username": "mattronix"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1424800284,
    "expire_in": 157680000,
    "prev": "b70f421c73ae2747ce50658e45f2d838bea2f2c148323431d55a917356fa14ab",
    "seqno": 14,
    "tag": "signature"
}
```

with the key [4B40 7AE9 F989 204F 4F96  92C4 3C82 E5A8 D2E8 DE01](https://keybase.io/mattronix), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v2

owF9Um1MFEcYBkGKoqJG6TW1FS9qINFjZ3b2C8GzFYIftYqi/aIhM7uzuAq3sLd3
ekFEqJqK0p4RidG0BmNzViK1pq0fVKVpFRC1iq3VEtRy2oJCU4piCrZ0j9iE9kf/
zbzzPM/7Pu8z/rERYdHhKWX3DrV03AoPb+4kYVldp6YU2Ymu+OzJRXY5T6MuM3Ry
4XxqT7avpT6C3dSh6fEuXaGONe74p5hZdi813JruslCMQ3AI9uJZIXiIrGquXGoU
GFpIy44IYgRMJVUSJcggFakSL0EZsbIIKYdFBVJRoQywJFfrbvNfXe1DmjmaYlXZ
+SJM514S02C6mJY+hPcMPSgyB6iCJcBCReF4XkWySDEEIsMAHjNMCOimxlNL+dg0
Dd2lrQ8NnE+NtXk0x9D1IdeyqYUwAEFkkSHPWCNh92qLJSFKEICIJaoCWCQCSigr
ExkAzGGWYCSKRGIRRQokEEmUwUCFLGKIqkocRFgBClQkKluOASexMmQp5SUGS7yg
qLIs8LyoyISIVLRmZwRFArIqUUVioIoUAWDLg5sWunRrOJ4RWLY4dDe8mkyHpZWr
mas95H/smr6CUG0dJTlD7I9lmkM0l2LlNTxQYEH/swoRzbLT9QWaQXO0EIITeKvM
WAsqMKjX0iQCoyIIZIHFFApIkCnH8JxIEadCRWRFYiWiQhkgkbX2wgKF46zEBJbj
VQwQJsMMWq1MnGtpurVcFzY9BrUXbxsxJTIsPDosauSI0JcNGz1q/D//ONEZO0i2
ve9pj/183x7flOO9y3/5oLMuMEdvA5HZh2Larq0b2fd9TPXk5tLYonk1WQ/bKwod
ebXizNn1CT3cmiXZC551dDeerh83d9knacHUlscbIxeDz775+slbN3+sqUmK816F
dSObi7s+CkYem5a5E8avCqzY0PrbilsJkRlnj3b/WZmpu7/YT4S+uNOeE+0rm76N
+yqGJC1atLA1qf7TwdmPLmV4fNk/v026Uq6k+Ccsu5Dw4sTSveNG7fIG7/YumVd+
veP6T/KvD/pbxsjFeFnsgi2nxp5evGj6yY5XfOUlsGpMepzj8lkFD5yKjxrlfVIh
vLv/QV3F3IapAf+kVf5xO3ybfLsqm1KrDxx82XbzmZnc9W22pVccFRERKbbCjPsH
VPD69NTxeem43Kx0Ru1p2GtLarqRVt1z6L0ZQvDewkb35Ydbd89ZOXlG/JlNJRdt
uaMbfcGTk5KeMxoy2VfpjYGlh6t72+44bQXndgbPf/mobGtZAAz4PlyeknHXNDf1
5S/oKfnuzdIItvtm6x+7Amd2bJzw/Pm+uh41QdqdHNw+mLI50LKh7cLW3tr6v7Kr
3hgY789NtgmvVW92XjQKDt8pldonguQj8+93Vk7Kug0j9yROq+3uv1aYQKr0fQfx
1GmFEZfK+2+DVu/jxC1q+K0Xym8fUY+mvXPiuNPpn3muqv+Hi79PP9ZwtTv6bw==
=z21l
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/mattronix

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id mattronix

# encrypt a message to me
keybase encrypt mattronix -m 'a secret message...'

# ...and more...
```

